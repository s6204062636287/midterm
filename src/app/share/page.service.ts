import { Injectable } from '@angular/core';
import { Flight } from '../flight';
import { MockFlight } from './mock-flight';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights : Flight[]=[];
  constructor() {
    this.flights= MockFlight.mflight;
  }
  getFlights() : Flight[]{
    return this.flights;
  }
  addFlights(f:Flight): void{
    this.flights.push(f);
  }
}
